include Makefile.pipeline
dcbash=docker-compose exec -T php-fpm bin/console $(command)

default: help

start: ## Start all containers
	@docker-compose up -d --build

stop: ## Stop all containers
	@docker-compose down -v

logs: ## Follow log output
	@docker-compose logs -f

verbose-logs: ## Tail verbose log
	@docker-compose exec php-fpm tail -f var/log/dev.log

kill-all: ## Kill all active containers (inside and outside the project)
	containers = docker ps -a -q
	docker stop $(containers)

bash: ## Bash into PHP container
	@docker-compose exec php-fpm bash

mysql: ## Bash into MySQL container
	@docker-compose exec mysql bash

rebuild: ## Rebuild and start all containers
	@docker-compose build
	@make start

composer-install: ## Run composer install
	@docker-compose exec -T php-fpm composer install

migrate: ## Run Doctrine migrations on prod and test databases
	$(dcbash) doctrine:database:create --if-not-exists --no-interaction
	$(dcbash) doctrine:migrations:migrate --no-interaction

load-fixtures: ## Replace database contents with test data
	$(dcbash) doctrine:fixtures:load

clear-cache: ## Clear cache
	$(dcbash) cache:clear
	$(dcbash) cache:clear --env=test

migrate-tests-data:
	$(dcbash) doctrine:migrations:migrate --env=test --no-interaction
	$(dcbash) doctrine:fixtures:load --env=test --no-interaction

run-all-tests: migrate-tests-data run-unit-tests run-behat-tests ## Run all tests (unit and behat)

run-unit-tests: migrate-tests-data ## Run all unit tests
	@docker-compose exec -T php-fpm bin/phpunit

run-specific-unit-test: ## Run specific unit test (path=tests/<path-to-your-unit-test>)
	@docker-compose exec php-fpm bin/phpunit $(path)

run-behat-tests: ## Run behat tests
	@docker-compose exec -T php-fpm vendor/bin/behat

routes: ## List all routes
	$(dcbash) debug:router

help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk -F":|.##|: ##" '{printf "\033[36m%-30s\033[0m %s \n", $$2, $$3}'
